library active_symbols_request;

import 'dart:convert';

import 'package:binary_mobile_app/model/requests/immutables/serializer.dart';
import 'package:binary_mobile_app/model/requests/request_base.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'active_symbols_request.g.dart';

abstract class ActiveSymbolsRequest
    implements Built<ActiveSymbolsRequest, ActiveSymbolsRequestBuilder> {
  ActiveSymbolsRequest._();

  factory ActiveSymbolsRequest([updates(ActiveSymbolsRequestBuilder b)]) =
  _$ActiveSymbolsRequest;

  @BuiltValueField(wireName: 'active_symbols')
  String get activeSymbols;
  @BuiltValueField(wireName: 'product_type')
  String get productType;
  @BuiltValueField(wireName: 'req_id')
  int get reqId;
  String toJson() {
    return json.encode(
        serializers.serializeWith(ActiveSymbolsRequest.serializer, this));
  }

  static ActiveSymbolsRequest fromJson(String jsonString) {
    return serializers.deserializeWith(
        ActiveSymbolsRequest.serializer, json.decode(jsonString));
  }

  static Serializer<ActiveSymbolsRequest> get serializer =>
      _$activeSymbolsRequestSerializer;
}
