// GENERATED CODE - DO NOT MODIFY BY HAND

part of active_symbols_request;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ActiveSymbolsRequest> _$activeSymbolsRequestSerializer =
    new _$ActiveSymbolsRequestSerializer();

class _$ActiveSymbolsRequestSerializer
    implements StructuredSerializer<ActiveSymbolsRequest> {
  @override
  final Iterable<Type> types = const [
    ActiveSymbolsRequest,
    _$ActiveSymbolsRequest
  ];
  @override
  final String wireName = 'ActiveSymbolsRequest';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ActiveSymbolsRequest object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'active_symbols',
      serializers.serialize(object.activeSymbols,
          specifiedType: const FullType(String)),
      'product_type',
      serializers.serialize(object.productType,
          specifiedType: const FullType(String)),
      'req_id',
      serializers.serialize(object.reqId, specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  ActiveSymbolsRequest deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ActiveSymbolsRequestBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'active_symbols':
          result.activeSymbols = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'product_type':
          result.productType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'req_id':
          result.reqId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$ActiveSymbolsRequest extends ActiveSymbolsRequest {
  @override
  final String activeSymbols;
  @override
  final String productType;
  @override
  final int reqId;

  factory _$ActiveSymbolsRequest(
          [void Function(ActiveSymbolsRequestBuilder) updates]) =>
      (new ActiveSymbolsRequestBuilder()..update(updates)).build();

  _$ActiveSymbolsRequest._({this.activeSymbols, this.productType, this.reqId})
      : super._() {
    if (activeSymbols == null) {
      throw new BuiltValueNullFieldError(
          'ActiveSymbolsRequest', 'activeSymbols');
    }
    if (productType == null) {
      throw new BuiltValueNullFieldError('ActiveSymbolsRequest', 'productType');
    }
    if (reqId == null) {
      throw new BuiltValueNullFieldError('ActiveSymbolsRequest', 'reqId');
    }
  }

  @override
  ActiveSymbolsRequest rebuild(
          void Function(ActiveSymbolsRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ActiveSymbolsRequestBuilder toBuilder() =>
      new ActiveSymbolsRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ActiveSymbolsRequest &&
        activeSymbols == other.activeSymbols &&
        productType == other.productType &&
        reqId == other.reqId;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, activeSymbols.hashCode), productType.hashCode),
        reqId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ActiveSymbolsRequest')
          ..add('activeSymbols', activeSymbols)
          ..add('productType', productType)
          ..add('reqId', reqId))
        .toString();
  }
}

class ActiveSymbolsRequestBuilder
    implements Builder<ActiveSymbolsRequest, ActiveSymbolsRequestBuilder> {
  _$ActiveSymbolsRequest _$v;

  String _activeSymbols;
  String get activeSymbols => _$this._activeSymbols;
  set activeSymbols(String activeSymbols) =>
      _$this._activeSymbols = activeSymbols;

  String _productType;
  String get productType => _$this._productType;
  set productType(String productType) => _$this._productType = productType;

  int _reqId;
  int get reqId => _$this._reqId;
  set reqId(int reqId) => _$this._reqId = reqId;

  ActiveSymbolsRequestBuilder();

  ActiveSymbolsRequestBuilder get _$this {
    if (_$v != null) {
      _activeSymbols = _$v.activeSymbols;
      _productType = _$v.productType;
      _reqId = _$v.reqId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ActiveSymbolsRequest other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ActiveSymbolsRequest;
  }

  @override
  void update(void Function(ActiveSymbolsRequestBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ActiveSymbolsRequest build() {
    final _$result = _$v ??
        new _$ActiveSymbolsRequest._(
            activeSymbols: activeSymbols,
            productType: productType,
            reqId: reqId);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
