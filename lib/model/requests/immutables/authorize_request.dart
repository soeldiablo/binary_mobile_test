library authorize_request;

import 'dart:convert';

import 'package:binary_mobile_app/model/requests/immutables/serializer.dart';
import 'package:binary_mobile_app/model/requests/request_base.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'authorize_request.g.dart';

abstract class AuthorizeRequest
    implements Built<AuthorizeRequest, AuthorizeRequestBuilder> {
  AuthorizeRequest._();

  factory AuthorizeRequest([updates(AuthorizeRequestBuilder b)]) =
  _$AuthorizeRequest;

  @BuiltValueField(wireName: 'authorize')
  String get authorize;
  @BuiltValueField(wireName: 'req_id')
  int get reqId;
  String toJson() {
    return json
        .encode(serializers.serializeWith(AuthorizeRequest.serializer, this));
  }

  static AuthorizeRequest fromJson(String jsonString) {
    return serializers.deserializeWith(
        AuthorizeRequest.serializer, json.decode(jsonString));
  }

  static Serializer<AuthorizeRequest> get serializer =>
      _$authorizeRequestSerializer;
}
