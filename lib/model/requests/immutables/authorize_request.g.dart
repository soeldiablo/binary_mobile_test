// GENERATED CODE - DO NOT MODIFY BY HAND

part of authorize_request;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<AuthorizeRequest> _$authorizeRequestSerializer =
    new _$AuthorizeRequestSerializer();

class _$AuthorizeRequestSerializer
    implements StructuredSerializer<AuthorizeRequest> {
  @override
  final Iterable<Type> types = const [AuthorizeRequest, _$AuthorizeRequest];
  @override
  final String wireName = 'AuthorizeRequest';

  @override
  Iterable<Object> serialize(Serializers serializers, AuthorizeRequest object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'authorize',
      serializers.serialize(object.authorize,
          specifiedType: const FullType(String)),
      'req_id',
      serializers.serialize(object.reqId, specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  AuthorizeRequest deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new AuthorizeRequestBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'authorize':
          result.authorize = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'req_id':
          result.reqId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$AuthorizeRequest extends AuthorizeRequest {
  @override
  final String authorize;
  @override
  final int reqId;

  factory _$AuthorizeRequest(
          [void Function(AuthorizeRequestBuilder) updates]) =>
      (new AuthorizeRequestBuilder()..update(updates)).build();

  _$AuthorizeRequest._({this.authorize, this.reqId}) : super._() {
    if (authorize == null) {
      throw new BuiltValueNullFieldError('AuthorizeRequest', 'authorize');
    }
    if (reqId == null) {
      throw new BuiltValueNullFieldError('AuthorizeRequest', 'reqId');
    }
  }

  @override
  AuthorizeRequest rebuild(void Function(AuthorizeRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AuthorizeRequestBuilder toBuilder() =>
      new AuthorizeRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AuthorizeRequest &&
        authorize == other.authorize &&
        reqId == other.reqId;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, authorize.hashCode), reqId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AuthorizeRequest')
          ..add('authorize', authorize)
          ..add('reqId', reqId))
        .toString();
  }
}

class AuthorizeRequestBuilder
    implements Builder<AuthorizeRequest, AuthorizeRequestBuilder> {
  _$AuthorizeRequest _$v;

  String _authorize;
  String get authorize => _$this._authorize;
  set authorize(String authorize) => _$this._authorize = authorize;

  int _reqId;
  int get reqId => _$this._reqId;
  set reqId(int reqId) => _$this._reqId = reqId;

  AuthorizeRequestBuilder();

  AuthorizeRequestBuilder get _$this {
    if (_$v != null) {
      _authorize = _$v.authorize;
      _reqId = _$v.reqId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AuthorizeRequest other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$AuthorizeRequest;
  }

  @override
  void update(void Function(AuthorizeRequestBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$AuthorizeRequest build() {
    final _$result =
        _$v ?? new _$AuthorizeRequest._(authorize: authorize, reqId: reqId);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
