
import 'package:binary_mobile_app/model/requests/immutables/active_symbols_request.dart';
import 'package:binary_mobile_app/model/requests/immutables/authorize_request.dart';
import 'package:binary_mobile_app/model/requests/request_base.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';

part 'serializer.g.dart';

@SerializersFor(const [ActiveSymbolsRequest, AuthorizeRequest])
final Serializers serializers = (_$serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();