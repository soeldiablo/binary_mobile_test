library request_base;

import 'dart:convert';

import 'package:binary_mobile_app/model/requests/serializer.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'request_base.g.dart';


abstract class RequestBase implements Built<RequestBase, RequestBaseBuilder> {
  RequestBase._();

  factory RequestBase([updates(RequestBaseBuilder b)]) = _$RequestBase;

  @BuiltValueField(wireName: 'req_id')
  int get reqId;

  String toJson() {
    return json.encode(serializers.serializeWith(RequestBase.serializer, this));
  }

  static RequestBase fromJson(String jsonString) {
    return serializers.deserializeWith(
        RequestBase.serializer, json.decode(jsonString));
  }

  static Serializer<RequestBase> get serializer => _$requestBaseSerializer;
}
