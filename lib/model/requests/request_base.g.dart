// GENERATED CODE - DO NOT MODIFY BY HAND

part of request_base;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<RequestBase> _$requestBaseSerializer = new _$RequestBaseSerializer();

class _$RequestBaseSerializer implements StructuredSerializer<RequestBase> {
  @override
  final Iterable<Type> types = const [RequestBase, _$RequestBase];
  @override
  final String wireName = 'RequestBase';

  @override
  Iterable<Object> serialize(Serializers serializers, RequestBase object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'req_id',
      serializers.serialize(object.reqId, specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  RequestBase deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new RequestBaseBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'req_id':
          result.reqId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$RequestBase extends RequestBase {
  @override
  final int reqId;

  factory _$RequestBase([void Function(RequestBaseBuilder) updates]) =>
      (new RequestBaseBuilder()..update(updates)).build();

  _$RequestBase._({this.reqId}) : super._() {
    if (reqId == null) {
      throw new BuiltValueNullFieldError('RequestBase', 'reqId');
    }
  }

  @override
  RequestBase rebuild(void Function(RequestBaseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RequestBaseBuilder toBuilder() => new RequestBaseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RequestBase && reqId == other.reqId;
  }

  @override
  int get hashCode {
    return $jf($jc(0, reqId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('RequestBase')..add('reqId', reqId))
        .toString();
  }
}

class RequestBaseBuilder implements Builder<RequestBase, RequestBaseBuilder> {
  _$RequestBase _$v;

  int _reqId;
  int get reqId => _$this._reqId;
  set reqId(int reqId) => _$this._reqId = reqId;

  RequestBaseBuilder();

  RequestBaseBuilder get _$this {
    if (_$v != null) {
      _reqId = _$v.reqId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RequestBase other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$RequestBase;
  }

  @override
  void update(void Function(RequestBaseBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$RequestBase build() {
    final _$result = _$v ?? new _$RequestBase._(reqId: reqId);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
