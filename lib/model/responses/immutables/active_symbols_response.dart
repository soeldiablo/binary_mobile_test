library active_symbols_response;

import 'dart:convert';

import 'package:binary_mobile_app/model/responses/immutables/serializer.dart';
import 'package:binary_mobile_app/model/responses/response_base.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'active_symbols_response.g.dart';

abstract class ActiveSymbolsResponse
    implements Built<ActiveSymbolsResponse, ActiveSymbolsResponseBuilder>, Response {
  ActiveSymbolsResponse._();

  factory ActiveSymbolsResponse([updates(ActiveSymbolsResponseBuilder b)]) =
  _$ActiveSymbolsResponse;

  @BuiltValueField(wireName: 'active_symbols')
  BuiltList<ActiveSymbols> get activeSymbols;
  @BuiltValueField(wireName: 'msg_type')
  String get msgType;
  @BuiltValueField(wireName: 'req_id')
  int get reqId;
  String toJson() {
    return json.encode(
        serializers.serializeWith(ActiveSymbolsResponse.serializer, this));
  }

  static ActiveSymbolsResponse fromJson(String jsonString) {
    return serializers.deserializeWith(
        ActiveSymbolsResponse.serializer, json.decode(jsonString));
  }

  static Serializer<ActiveSymbolsResponse> get serializer =>
      _$activeSymbolsResponseSerializer;
}

abstract class ActiveSymbols
    implements Built<ActiveSymbols, ActiveSymbolsBuilder> {
  ActiveSymbols._();

  factory ActiveSymbols([updates(ActiveSymbolsBuilder b)]) = _$ActiveSymbols;

  @BuiltValueField(wireName: 'allow_forward_starting')
  int get allowForwardStarting;
  @BuiltValueField(wireName: 'display_name')
  String get displayName;
  @BuiltValueField(wireName: 'exchange_is_open')
  int get exchangeIsOpen;
  @BuiltValueField(wireName: 'is_trading_suspended')
  int get isTradingSuspended;
  @BuiltValueField(wireName: 'market')
  String get market;
  @BuiltValueField(wireName: 'market_display_name')
  String get marketDisplayName;
  @BuiltValueField(wireName: 'pip')
  double get pip;
  @BuiltValueField(wireName: 'submarket')
  String get submarket;
  @BuiltValueField(wireName: 'submarket_display_name')
  String get submarketDisplayName;
  @BuiltValueField(wireName: 'symbol')
  String get symbol;
  @BuiltValueField(wireName: 'symbol_type')
  String get symbolType;
  String toJson() {
    return json
        .encode(serializers.serializeWith(ActiveSymbols.serializer, this));
  }

  static ActiveSymbols fromJson(String jsonString) {
    return serializers.deserializeWith(
        ActiveSymbols.serializer, json.decode(jsonString));
  }

  static Serializer<ActiveSymbols> get serializer => _$activeSymbolsSerializer;
}
