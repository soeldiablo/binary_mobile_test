// GENERATED CODE - DO NOT MODIFY BY HAND

part of active_symbols_response;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ActiveSymbolsResponse> _$activeSymbolsResponseSerializer =
    new _$ActiveSymbolsResponseSerializer();
Serializer<ActiveSymbols> _$activeSymbolsSerializer =
    new _$ActiveSymbolsSerializer();

class _$ActiveSymbolsResponseSerializer
    implements StructuredSerializer<ActiveSymbolsResponse> {
  @override
  final Iterable<Type> types = const [
    ActiveSymbolsResponse,
    _$ActiveSymbolsResponse
  ];
  @override
  final String wireName = 'ActiveSymbolsResponse';

  @override
  Iterable<Object> serialize(
      Serializers serializers, ActiveSymbolsResponse object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'active_symbols',
      serializers.serialize(object.activeSymbols,
          specifiedType:
              const FullType(BuiltList, const [const FullType(ActiveSymbols)])),
      'msg_type',
      serializers.serialize(object.msgType,
          specifiedType: const FullType(String)),
      'req_id',
      serializers.serialize(object.reqId, specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  ActiveSymbolsResponse deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ActiveSymbolsResponseBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'active_symbols':
          result.activeSymbols.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(ActiveSymbols)]))
              as BuiltList<dynamic>);
          break;
        case 'msg_type':
          result.msgType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'req_id':
          result.reqId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$ActiveSymbolsSerializer implements StructuredSerializer<ActiveSymbols> {
  @override
  final Iterable<Type> types = const [ActiveSymbols, _$ActiveSymbols];
  @override
  final String wireName = 'ActiveSymbols';

  @override
  Iterable<Object> serialize(Serializers serializers, ActiveSymbols object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'allow_forward_starting',
      serializers.serialize(object.allowForwardStarting,
          specifiedType: const FullType(int)),
      'display_name',
      serializers.serialize(object.displayName,
          specifiedType: const FullType(String)),
      'exchange_is_open',
      serializers.serialize(object.exchangeIsOpen,
          specifiedType: const FullType(int)),
      'is_trading_suspended',
      serializers.serialize(object.isTradingSuspended,
          specifiedType: const FullType(int)),
      'market',
      serializers.serialize(object.market,
          specifiedType: const FullType(String)),
      'market_display_name',
      serializers.serialize(object.marketDisplayName,
          specifiedType: const FullType(String)),
      'pip',
      serializers.serialize(object.pip, specifiedType: const FullType(double)),
      'submarket',
      serializers.serialize(object.submarket,
          specifiedType: const FullType(String)),
      'submarket_display_name',
      serializers.serialize(object.submarketDisplayName,
          specifiedType: const FullType(String)),
      'symbol',
      serializers.serialize(object.symbol,
          specifiedType: const FullType(String)),
      'symbol_type',
      serializers.serialize(object.symbolType,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  ActiveSymbols deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ActiveSymbolsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'allow_forward_starting':
          result.allowForwardStarting = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'display_name':
          result.displayName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'exchange_is_open':
          result.exchangeIsOpen = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'is_trading_suspended':
          result.isTradingSuspended = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'market':
          result.market = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'market_display_name':
          result.marketDisplayName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'pip':
          result.pip = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'submarket':
          result.submarket = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'submarket_display_name':
          result.submarketDisplayName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'symbol':
          result.symbol = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'symbol_type':
          result.symbolType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$ActiveSymbolsResponse extends ActiveSymbolsResponse {
  @override
  final BuiltList<ActiveSymbols> activeSymbols;
  @override
  final String msgType;
  @override
  final int reqId;

  factory _$ActiveSymbolsResponse(
          [void Function(ActiveSymbolsResponseBuilder) updates]) =>
      (new ActiveSymbolsResponseBuilder()..update(updates)).build();

  _$ActiveSymbolsResponse._({this.activeSymbols, this.msgType, this.reqId})
      : super._() {
    if (activeSymbols == null) {
      throw new BuiltValueNullFieldError(
          'ActiveSymbolsResponse', 'activeSymbols');
    }
    if (msgType == null) {
      throw new BuiltValueNullFieldError('ActiveSymbolsResponse', 'msgType');
    }
    if (reqId == null) {
      throw new BuiltValueNullFieldError('ActiveSymbolsResponse', 'reqId');
    }
  }

  @override
  ActiveSymbolsResponse rebuild(
          void Function(ActiveSymbolsResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ActiveSymbolsResponseBuilder toBuilder() =>
      new ActiveSymbolsResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ActiveSymbolsResponse &&
        activeSymbols == other.activeSymbols &&
        msgType == other.msgType &&
        reqId == other.reqId;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, activeSymbols.hashCode), msgType.hashCode), reqId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ActiveSymbolsResponse')
          ..add('activeSymbols', activeSymbols)
          ..add('msgType', msgType)
          ..add('reqId', reqId))
        .toString();
  }
}

class ActiveSymbolsResponseBuilder
    implements Builder<ActiveSymbolsResponse, ActiveSymbolsResponseBuilder> {
  _$ActiveSymbolsResponse _$v;

  ListBuilder<ActiveSymbols> _activeSymbols;
  ListBuilder<ActiveSymbols> get activeSymbols =>
      _$this._activeSymbols ??= new ListBuilder<ActiveSymbols>();
  set activeSymbols(ListBuilder<ActiveSymbols> activeSymbols) =>
      _$this._activeSymbols = activeSymbols;

  String _msgType;
  String get msgType => _$this._msgType;
  set msgType(String msgType) => _$this._msgType = msgType;

  int _reqId;
  int get reqId => _$this._reqId;
  set reqId(int reqId) => _$this._reqId = reqId;

  ActiveSymbolsResponseBuilder();

  ActiveSymbolsResponseBuilder get _$this {
    if (_$v != null) {
      _activeSymbols = _$v.activeSymbols?.toBuilder();
      _msgType = _$v.msgType;
      _reqId = _$v.reqId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ActiveSymbolsResponse other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ActiveSymbolsResponse;
  }

  @override
  void update(void Function(ActiveSymbolsResponseBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ActiveSymbolsResponse build() {
    _$ActiveSymbolsResponse _$result;
    try {
      _$result = _$v ??
          new _$ActiveSymbolsResponse._(
              activeSymbols: activeSymbols.build(),
              msgType: msgType,
              reqId: reqId);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'activeSymbols';
        activeSymbols.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ActiveSymbolsResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$ActiveSymbols extends ActiveSymbols {
  @override
  final int allowForwardStarting;
  @override
  final String displayName;
  @override
  final int exchangeIsOpen;
  @override
  final int isTradingSuspended;
  @override
  final String market;
  @override
  final String marketDisplayName;
  @override
  final double pip;
  @override
  final String submarket;
  @override
  final String submarketDisplayName;
  @override
  final String symbol;
  @override
  final String symbolType;

  factory _$ActiveSymbols([void Function(ActiveSymbolsBuilder) updates]) =>
      (new ActiveSymbolsBuilder()..update(updates)).build();

  _$ActiveSymbols._(
      {this.allowForwardStarting,
      this.displayName,
      this.exchangeIsOpen,
      this.isTradingSuspended,
      this.market,
      this.marketDisplayName,
      this.pip,
      this.submarket,
      this.submarketDisplayName,
      this.symbol,
      this.symbolType})
      : super._() {
    if (allowForwardStarting == null) {
      throw new BuiltValueNullFieldError(
          'ActiveSymbols', 'allowForwardStarting');
    }
    if (displayName == null) {
      throw new BuiltValueNullFieldError('ActiveSymbols', 'displayName');
    }
    if (exchangeIsOpen == null) {
      throw new BuiltValueNullFieldError('ActiveSymbols', 'exchangeIsOpen');
    }
    if (isTradingSuspended == null) {
      throw new BuiltValueNullFieldError('ActiveSymbols', 'isTradingSuspended');
    }
    if (market == null) {
      throw new BuiltValueNullFieldError('ActiveSymbols', 'market');
    }
    if (marketDisplayName == null) {
      throw new BuiltValueNullFieldError('ActiveSymbols', 'marketDisplayName');
    }
    if (pip == null) {
      throw new BuiltValueNullFieldError('ActiveSymbols', 'pip');
    }
    if (submarket == null) {
      throw new BuiltValueNullFieldError('ActiveSymbols', 'submarket');
    }
    if (submarketDisplayName == null) {
      throw new BuiltValueNullFieldError(
          'ActiveSymbols', 'submarketDisplayName');
    }
    if (symbol == null) {
      throw new BuiltValueNullFieldError('ActiveSymbols', 'symbol');
    }
    if (symbolType == null) {
      throw new BuiltValueNullFieldError('ActiveSymbols', 'symbolType');
    }
  }

  @override
  ActiveSymbols rebuild(void Function(ActiveSymbolsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ActiveSymbolsBuilder toBuilder() => new ActiveSymbolsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ActiveSymbols &&
        allowForwardStarting == other.allowForwardStarting &&
        displayName == other.displayName &&
        exchangeIsOpen == other.exchangeIsOpen &&
        isTradingSuspended == other.isTradingSuspended &&
        market == other.market &&
        marketDisplayName == other.marketDisplayName &&
        pip == other.pip &&
        submarket == other.submarket &&
        submarketDisplayName == other.submarketDisplayName &&
        symbol == other.symbol &&
        symbolType == other.symbolType;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(0,
                                                allowForwardStarting.hashCode),
                                            displayName.hashCode),
                                        exchangeIsOpen.hashCode),
                                    isTradingSuspended.hashCode),
                                market.hashCode),
                            marketDisplayName.hashCode),
                        pip.hashCode),
                    submarket.hashCode),
                submarketDisplayName.hashCode),
            symbol.hashCode),
        symbolType.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ActiveSymbols')
          ..add('allowForwardStarting', allowForwardStarting)
          ..add('displayName', displayName)
          ..add('exchangeIsOpen', exchangeIsOpen)
          ..add('isTradingSuspended', isTradingSuspended)
          ..add('market', market)
          ..add('marketDisplayName', marketDisplayName)
          ..add('pip', pip)
          ..add('submarket', submarket)
          ..add('submarketDisplayName', submarketDisplayName)
          ..add('symbol', symbol)
          ..add('symbolType', symbolType))
        .toString();
  }
}

class ActiveSymbolsBuilder
    implements Builder<ActiveSymbols, ActiveSymbolsBuilder> {
  _$ActiveSymbols _$v;

  int _allowForwardStarting;
  int get allowForwardStarting => _$this._allowForwardStarting;
  set allowForwardStarting(int allowForwardStarting) =>
      _$this._allowForwardStarting = allowForwardStarting;

  String _displayName;
  String get displayName => _$this._displayName;
  set displayName(String displayName) => _$this._displayName = displayName;

  int _exchangeIsOpen;
  int get exchangeIsOpen => _$this._exchangeIsOpen;
  set exchangeIsOpen(int exchangeIsOpen) =>
      _$this._exchangeIsOpen = exchangeIsOpen;

  int _isTradingSuspended;
  int get isTradingSuspended => _$this._isTradingSuspended;
  set isTradingSuspended(int isTradingSuspended) =>
      _$this._isTradingSuspended = isTradingSuspended;

  String _market;
  String get market => _$this._market;
  set market(String market) => _$this._market = market;

  String _marketDisplayName;
  String get marketDisplayName => _$this._marketDisplayName;
  set marketDisplayName(String marketDisplayName) =>
      _$this._marketDisplayName = marketDisplayName;

  double _pip;
  double get pip => _$this._pip;
  set pip(double pip) => _$this._pip = pip;

  String _submarket;
  String get submarket => _$this._submarket;
  set submarket(String submarket) => _$this._submarket = submarket;

  String _submarketDisplayName;
  String get submarketDisplayName => _$this._submarketDisplayName;
  set submarketDisplayName(String submarketDisplayName) =>
      _$this._submarketDisplayName = submarketDisplayName;

  String _symbol;
  String get symbol => _$this._symbol;
  set symbol(String symbol) => _$this._symbol = symbol;

  String _symbolType;
  String get symbolType => _$this._symbolType;
  set symbolType(String symbolType) => _$this._symbolType = symbolType;

  ActiveSymbolsBuilder();

  ActiveSymbolsBuilder get _$this {
    if (_$v != null) {
      _allowForwardStarting = _$v.allowForwardStarting;
      _displayName = _$v.displayName;
      _exchangeIsOpen = _$v.exchangeIsOpen;
      _isTradingSuspended = _$v.isTradingSuspended;
      _market = _$v.market;
      _marketDisplayName = _$v.marketDisplayName;
      _pip = _$v.pip;
      _submarket = _$v.submarket;
      _submarketDisplayName = _$v.submarketDisplayName;
      _symbol = _$v.symbol;
      _symbolType = _$v.symbolType;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ActiveSymbols other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ActiveSymbols;
  }

  @override
  void update(void Function(ActiveSymbolsBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ActiveSymbols build() {
    final _$result = _$v ??
        new _$ActiveSymbols._(
            allowForwardStarting: allowForwardStarting,
            displayName: displayName,
            exchangeIsOpen: exchangeIsOpen,
            isTradingSuspended: isTradingSuspended,
            market: market,
            marketDisplayName: marketDisplayName,
            pip: pip,
            submarket: submarket,
            submarketDisplayName: submarketDisplayName,
            symbol: symbol,
            symbolType: symbolType);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
