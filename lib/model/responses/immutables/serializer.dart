
import 'package:binary_mobile_app/model/responses/immutables/active_symbols_response.dart';
import 'package:binary_mobile_app/model/responses/response_base.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:built_collection/built_collection.dart';

part 'serializer.g.dart';

@SerializersFor(const [ActiveSymbolsResponse,])
final Serializers serializers = (_$serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();