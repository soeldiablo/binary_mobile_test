library response_base;

import 'dart:convert';

import 'package:binary_mobile_app/model/responses/serializer.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'response_base.g.dart';

abstract class Response {
  String get msgType;
  int get reqId;
}

abstract class ResponseBase
    implements Built<ResponseBase, ResponseBaseBuilder>, Response {
  ResponseBase._();

  factory ResponseBase([updates(ResponseBaseBuilder b)]) = _$ResponseBase;

  @nullable
  @BuiltValueField(wireName: 'error')
  Error get error;
  @BuiltValueField(wireName: 'msg_type')
  String get msgType;
  @BuiltValueField(wireName: 'req_id')
  int get reqId;
  String toJson() {
    return json
        .encode(serializers.serializeWith(ResponseBase.serializer, this));
  }

  static ResponseBase fromJson(String jsonString) {
    return serializers.deserializeWith(
        ResponseBase.serializer, json.decode(jsonString));
  }

  static Serializer<ResponseBase> get serializer => _$responseBaseSerializer;
}

abstract class Error implements Built<Error, ErrorBuilder> {
  Error._();

  factory Error([updates(ErrorBuilder b)]) = _$Error;

  @BuiltValueField(wireName: 'code')
  String get code;
  @BuiltValueField(wireName: 'message')
  String get message;
  String toJson() {
    return json.encode(serializers.serializeWith(Error.serializer, this));
  }

  static Error fromJson(String jsonString) {
    return serializers.deserializeWith(
        Error.serializer, json.decode(jsonString));
  }

  static Serializer<Error> get serializer => _$errorSerializer;
}
