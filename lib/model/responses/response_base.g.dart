// GENERATED CODE - DO NOT MODIFY BY HAND

part of response_base;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ResponseBase> _$responseBaseSerializer =
    new _$ResponseBaseSerializer();
Serializer<Error> _$errorSerializer = new _$ErrorSerializer();

class _$ResponseBaseSerializer implements StructuredSerializer<ResponseBase> {
  @override
  final Iterable<Type> types = const [ResponseBase, _$ResponseBase];
  @override
  final String wireName = 'ResponseBase';

  @override
  Iterable<Object> serialize(Serializers serializers, ResponseBase object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'msg_type',
      serializers.serialize(object.msgType,
          specifiedType: const FullType(String)),
      'req_id',
      serializers.serialize(object.reqId, specifiedType: const FullType(int)),
    ];
    if (object.error != null) {
      result
        ..add('error')
        ..add(serializers.serialize(object.error,
            specifiedType: const FullType(Error)));
    }
    return result;
  }

  @override
  ResponseBase deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ResponseBaseBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'error':
          result.error.replace(serializers.deserialize(value,
              specifiedType: const FullType(Error)) as Error);
          break;
        case 'msg_type':
          result.msgType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'req_id':
          result.reqId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$ErrorSerializer implements StructuredSerializer<Error> {
  @override
  final Iterable<Type> types = const [Error, _$Error];
  @override
  final String wireName = 'Error';

  @override
  Iterable<Object> serialize(Serializers serializers, Error object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'code',
      serializers.serialize(object.code, specifiedType: const FullType(String)),
      'message',
      serializers.serialize(object.message,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  Error deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ErrorBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'code':
          result.code = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'message':
          result.message = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$ResponseBase extends ResponseBase {
  @override
  final Error error;
  @override
  final String msgType;
  @override
  final int reqId;

  factory _$ResponseBase([void Function(ResponseBaseBuilder) updates]) =>
      (new ResponseBaseBuilder()..update(updates)).build();

  _$ResponseBase._({this.error, this.msgType, this.reqId}) : super._() {
    if (msgType == null) {
      throw new BuiltValueNullFieldError('ResponseBase', 'msgType');
    }
    if (reqId == null) {
      throw new BuiltValueNullFieldError('ResponseBase', 'reqId');
    }
  }

  @override
  ResponseBase rebuild(void Function(ResponseBaseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ResponseBaseBuilder toBuilder() => new ResponseBaseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ResponseBase &&
        error == other.error &&
        msgType == other.msgType &&
        reqId == other.reqId;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, error.hashCode), msgType.hashCode), reqId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ResponseBase')
          ..add('error', error)
          ..add('msgType', msgType)
          ..add('reqId', reqId))
        .toString();
  }
}

class ResponseBaseBuilder
    implements Builder<ResponseBase, ResponseBaseBuilder> {
  _$ResponseBase _$v;

  ErrorBuilder _error;
  ErrorBuilder get error => _$this._error ??= new ErrorBuilder();
  set error(ErrorBuilder error) => _$this._error = error;

  String _msgType;
  String get msgType => _$this._msgType;
  set msgType(String msgType) => _$this._msgType = msgType;

  int _reqId;
  int get reqId => _$this._reqId;
  set reqId(int reqId) => _$this._reqId = reqId;

  ResponseBaseBuilder();

  ResponseBaseBuilder get _$this {
    if (_$v != null) {
      _error = _$v.error?.toBuilder();
      _msgType = _$v.msgType;
      _reqId = _$v.reqId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ResponseBase other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ResponseBase;
  }

  @override
  void update(void Function(ResponseBaseBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ResponseBase build() {
    _$ResponseBase _$result;
    try {
      _$result = _$v ??
          new _$ResponseBase._(
              error: _error?.build(), msgType: msgType, reqId: reqId);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'error';
        _error?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ResponseBase', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$Error extends Error {
  @override
  final String code;
  @override
  final String message;

  factory _$Error([void Function(ErrorBuilder) updates]) =>
      (new ErrorBuilder()..update(updates)).build();

  _$Error._({this.code, this.message}) : super._() {
    if (code == null) {
      throw new BuiltValueNullFieldError('Error', 'code');
    }
    if (message == null) {
      throw new BuiltValueNullFieldError('Error', 'message');
    }
  }

  @override
  Error rebuild(void Function(ErrorBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ErrorBuilder toBuilder() => new ErrorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Error && code == other.code && message == other.message;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, code.hashCode), message.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Error')
          ..add('code', code)
          ..add('message', message))
        .toString();
  }
}

class ErrorBuilder implements Builder<Error, ErrorBuilder> {
  _$Error _$v;

  String _code;
  String get code => _$this._code;
  set code(String code) => _$this._code = code;

  String _message;
  String get message => _$this._message;
  set message(String message) => _$this._message = message;

  ErrorBuilder();

  ErrorBuilder get _$this {
    if (_$v != null) {
      _code = _$v.code;
      _message = _$v.message;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Error other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Error;
  }

  @override
  void update(void Function(ErrorBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Error build() {
    final _$result = _$v ?? new _$Error._(code: code, message: message);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
