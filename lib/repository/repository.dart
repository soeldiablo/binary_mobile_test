


import 'package:binary_mobile_app/model/responses/immutables/active_symbols_response.dart';

abstract class Repository {
  // authorizes and gets the account information
  Future<ActiveSymbolsResponse> authorize();
}


