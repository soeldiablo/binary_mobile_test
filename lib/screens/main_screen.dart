import 'package:binary_mobile_app/viewmodels/main_screen_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Binary.com'),
      ),
      body: MultiProvider(
        providers: [
          ChangeNotifierProvider(builder: (_) => MainScreenViewModel()),
        ],
        child: Content(),
      ),
    );
  }
}

class Content extends StatefulWidget {
  @override
  _ContentState createState() => _ContentState();
}

class _ContentState extends State<Content> {
  @override
  Widget build(BuildContext context) {
    var mainScreenViewModel = Provider.of<MainScreenViewModel>(context);
    return Center(
      child: Column(
        children: <Widget>[
          RaisedButton(
            child: Text('active symbols'),
            onPressed: () {
              mainScreenViewModel.getActiveSymbols();
            },
          ),
          RaisedButton(
            child: Text('authorize'),
            onPressed: () {
              mainScreenViewModel.authorize();
            },
          ),
        ],
      ),
    );
  }
}
