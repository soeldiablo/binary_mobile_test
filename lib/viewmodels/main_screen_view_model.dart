import 'package:binary_mobile_app/model/requests/immutables/active_symbols_request.dart';
import 'package:binary_mobile_app/model/requests/immutables/authorize_request.dart';
import 'package:binary_mobile_app/model/responses/immutables/active_symbols_response.dart';
import 'package:binary_mobile_app/model/responses/response_base.dart';
import 'package:binary_mobile_app/repository/api_repo/binary_api.dart';
import 'package:binary_mobile_app/repository/repository.dart';
import 'package:flutter/material.dart';

class MainScreenViewModel extends ChangeNotifier{
  bool _loading = false;
  
  BinaryApi _api;

  MainScreenViewModel(){
    _api = BinaryApi.getInstance;
  }
  
  
  bool get isLoading {
    return _loading;
  }
  
  _setLoading(bool loading){
    _loading = loading;
  }
  
  getActiveSymbols() async{
    _setLoading(true);
    ActiveSymbolsRequest activeSymbolsRequest = ActiveSymbolsRequest((asr) {
      asr..reqId = 1
          ..productType = 'basic'
          ..activeSymbols = 'brief';
    });
    _api.sendRequest(activeSymbolsRequest.toJson(), activeSymbolsRequest.reqId, (Response response){
      var activeSymbols = response as ActiveSymbolsResponse;
      print("ddddd ${activeSymbols.activeSymbols}");
      _setLoading(false);
    });
    
  }

  authorize() async{
    _setLoading(true);
    AuthorizeRequest authorizeRequest = AuthorizeRequest((ar){
      ar.reqId = 2;
      ar.authorize = "KxsXtLNthWLmTpW";
    });
    _api.sendRequest(authorizeRequest.toJson(), authorizeRequest.reqId, (Response response){
      print("Authorized");
      _setLoading(false);
    });

  }
  
  
}