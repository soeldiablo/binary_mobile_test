import 'package:binary_mobile_app/model/responses/immutables/active_symbols_response.dart';
import 'package:binary_mobile_app/model/responses/response_base.dart';
import 'package:binary_mobile_app/viewmodels/main_screen_view_model.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('Built Value Account', () async {



  });

  test('Built Value Response Base', () async {
    var response  = "{\"active_symbols\": [{\"allow_forward_starting\": 0,\"display_name\": \"AUD Index\",\"exchange_is_open\": 1,\"is_trading_suspended\": 0,\"market\": \"forex\",\"market_display_name\": \"Forex\",\"pip\": 0.001,\"submarket\": \"smart_fx\",\"submarket_display_name\": \"Smart FX\",\"symbol\": \"WLDAUD\",\"symbol_type\": \"smart_fx\"}],\"msg_type\": \"active_symbols\",\"req_id\": 2}";


    ResponseBase responseBase = ResponseBase.fromJson(response);

    print("${responseBase.msgType}   ${responseBase.reqId}");

    ActiveSymbolsResponse activeSymbolsResponse = ActiveSymbolsResponse.fromJson(response);

    print("${activeSymbolsResponse.reqId}");

    print("${activeSymbolsResponse.activeSymbols.map((s){
      print(s.displayName);
    })}");

  });


  test('Active symbols test built value', () async {

    MainScreenViewModel viewModel = MainScreenViewModel();

    viewModel.getActiveSymbols();

  });

}


/*
{"active_symbols": [{"allow_forward_starting": 0,"display_name": "AUD Index","exchange_is_open": 1,"is_trading_suspended": 0,"market": "forex","market_display_name": "Forex","pip": 0.001,"submarket": "smart_fx","submarket_display_name": "Smart FX","symbol": "WLDAUD","symbol_type": "smart_fx"}],"msg_type": "active_symbols","req_id": 2}
 */